﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace FirstQuad.Common.Helpers
{
    public static class AttributeHelper
    {
        public static T GetAttribute<T>(this ICustomAttributeProvider provider)
            where T : Attribute
        {
            var attributes = provider.GetCustomAttributes(typeof(T), true);
            return attributes.Length > 0 ? attributes[0] as T : null;
        }

        public static Dictionary<string, string> GetEntityDisplayNames(Type type)
        {
            var dict = new Dictionary<string, string>();
            var props = type.GetProperties();

            foreach (var prop in props)
            {
                var displayName = GetPropertyDisplayName(prop);
                dict.Add(prop.Name, displayName);
            }
            return dict;
        }

        public static string GetPropertyDisplayName(MemberInfo p)
        {
            var displayName = p.Name;
            var displayNameAttr = p.GetCustomAttribute<DisplayNameAttribute>();
            if (displayNameAttr != null)
                displayName = displayNameAttr.DisplayName;

            return displayName;
        }

    }
}
