﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace FirstQuad.Common.Helpers
{
    public static class AssemblyHelper
    {
        public static Assembly[] GetAllReferensedAssemblies(string startNamespaceFilter)
        {
            var assemblies = new List<Assembly>();
            var currentAssemblies = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.GetName().Name.StartsWith(startNamespaceFilter)).ToList();

            while (currentAssemblies.Any())
            {
                assemblies.AddRange(currentAssemblies);
                var nextAssemblies = new List<Assembly>();
                foreach (var assembly in currentAssemblies)
                {
                    var assemblyNames = assembly.GetReferencedAssemblies().Where(a => a.Name.StartsWith(startNamespaceFilter));
                    foreach (var assemblyName in assemblyNames)
                    {
                        nextAssemblies.Add(Assembly.Load(assemblyName));
                    }
                }
                currentAssemblies = nextAssemblies;
            }

            return assemblies.Distinct().ToArray();
        }

        public static List<Type> GetReferencedTypes(Type baseType, string startNamespaceFilter)
        {
            var result = GetAllReferensedAssemblies(startNamespaceFilter)
                .SelectMany(a => a.GetTypes())
                .Where(type => baseType.IsAssignableFrom(type) && !type.IsAbstract);

            return result.ToList();
        }

        public static List<Type> GetReferencedTypes<T>(string startNamespaceFilter)
        {
            return GetReferencedTypes(typeof(T), startNamespaceFilter);
        }

        public static List<T> GetReferencedObjects<T>(string startNamespaceFilter, params object[] customConstructorParameters)
        {
            var types = GetReferencedTypes<T>(startNamespaceFilter);
            var result = new List<T>();

            foreach (var type in types)
            {
                var objectInstance = (T)Activator.CreateInstance(type, customConstructorParameters);
                result.Add(objectInstance);
            }

            return result;
        }

        public static List<Type> GetReferencedInterfaces(string startNamespaceFilter, Type interfaceType)
        {
            var result = GetAllReferensedAssemblies(startNamespaceFilter)
                .SelectMany(a => a.GetTypes())
                .Where(type => interfaceType.IsAssignableFrom(type) && type.IsInterface && type != interfaceType);

            return result.ToList();
        }
    }
}
