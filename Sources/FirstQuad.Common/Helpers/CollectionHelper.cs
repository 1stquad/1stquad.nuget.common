﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstQuad.Common.Enums;

namespace FirstQuad.Common.Helpers
{
    public static class CollectionHelper
    {
        public static void AddRange<T>(this ICollection<T> target, IEnumerable<T> source)
        {
            if (target == null)
                throw new ArgumentNullException(nameof(target));
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            foreach (var element in source)
                target.Add(element);
        }

        public static bool ContainsSequence<T>(this List<T> outer, List<T> inner)
        {
            var innerCount = inner.Count;

            for (var i = 0; i < outer.Count - innerCount; i++)
            {
                var isMatch = true;
                for (var x = 0; x < innerCount; x++)
                {
                    if (outer[i + x].Equals(inner[x]))
                        continue;

                    isMatch = false;
                    break;
                }

                if (isMatch)
                    return true;
            }

            return false;
        }


        /// <summary>
        /// Updates collection according to entityIds set
        /// </summary>
        /// <typeparam name="TEntity">Type of entity</typeparam>
        /// <typeparam name="TKey">Type of Key</typeparam>
        /// <param name="dbSet">Set to manage entities</param>
        /// <param name="entityIds">IDs to ensure in collection</param>
        /// <param name="entityFunc">Function to resolve entity by Id</param>
        /// <param name="onAdd">Event is called when entity is added to collection</param>
        /// <param name="onRemove">Event is called when entity is removed to collection</param>
        /// <param name="filter">Filter to manage just part of original collection</param>
        /// <param name="comparer">Compare 2 entities to match them. By default takes "Id" field by reflection</param>
        /// <param name="type">Type of managing: adding / removing / both (by default)</param>
        public static void ManageCollection<TEntity, TKey>(this ICollection<TEntity> dbSet, IEnumerable<TKey> entityIds, Func<TKey, TEntity> entityFunc,
            Action<TEntity> onAdd = null,
            Action<TEntity> onRemove = null,
            Func<TEntity, bool> filter = null,
            Func<TEntity, TEntity, bool> comparer = null,
            CollectionManageType type = CollectionManageType.All)
            where TEntity : class
        {
            if (filter == null)
                filter = entity => true;

            var oldEntities = dbSet.Where(filter).ToList();

            if (comparer == null)
            {
                comparer = (entityA, entityB) =>
                {
                    var propertyInfo = typeof(TEntity).GetProperty("Id");
                    if (propertyInfo == null)
                        throw new ArgumentException($"Can't find property Id in entity type {typeof(TEntity).FullName}, use selector parameter for manual key resolving");

                    return propertyInfo.GetValue(entityA).Equals(propertyInfo.GetValue(entityB));
                };
            }

            foreach (var entityId in entityIds)
            {
                var entity = entityFunc(entityId);
                if (entity == null)
                    throw new InvalidProgramException($"Can't find entity with id = {entityId}");

                var oldEntity = oldEntities.FirstOrDefault(x => comparer(x, entity));
                if (oldEntity == null)
                {
                    if ((type & CollectionManageType.Add) == CollectionManageType.Add)
                    {
                        dbSet.Add(entity);
                        onAdd?.Invoke(entity);
                    }
                }
                else
                {
                    oldEntities.Remove(entity);
                }
            }

            if ((type & CollectionManageType.Remove) == CollectionManageType.Remove)
            {
                foreach (var oldEntity in oldEntities)
                {
                    onRemove?.Invoke(oldEntity);
                    dbSet.Remove(oldEntity);
                }
            }
        }

        public static void ManageCollection<TEntity>(this ICollection<TEntity> dbSet, string commaSeparatedIdList, Func<int, TEntity> entityFunc,
            Action<TEntity> onAdd = null,
            Action<TEntity> onRemove = null,
            Func<TEntity, bool> filter = null,
            Func<TEntity, TEntity, bool> comparer = null)
            where TEntity : class
        {
            var entityIds = commaSeparatedIdList.GetValueOrEmpty().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();

            ManageCollection<TEntity, int>(dbSet, entityIds, entityFunc, onAdd, onRemove, filter, comparer);
        }

        public static void ManageCollection<TEntity>(this ICollection<TEntity> dbSet, string commaSeparatedIdList, Func<string, TEntity> entityFunc,
            Action<TEntity> onAdd = null,
            Action<TEntity> onRemove = null)
            where TEntity : class
        {
            var entityIds = commaSeparatedIdList.GetValueOrEmpty().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToArray();

            ManageCollection(dbSet, entityIds, entityFunc, onAdd, onRemove);
        }
    }
}
