﻿#if NET_4_5
using System.Web.Script.Serialization;
#endif
#if NETSTANDARD2_0
using Newtonsoft.Json;
#endif

namespace FirstQuad.Common.Helpers
{
    public class JsonHelper
    {
        public static string GetJsonFromObject(object obj)
        {
#if NETSTANDARD2_0
            return JsonConvert.SerializeObject(obj);
#endif
#if NET_4_5
            var sb = new System.Text.StringBuilder();
            var jsSerializer = new JavaScriptSerializer();

            jsSerializer.Serialize(obj, sb);
            return sb.ToString();
#endif
        }

        public static T GetObjectFromJson<T>(string json)
        {
#if NETSTANDARD2_0
            return JsonConvert.DeserializeObject<T>(json);
#endif
#if NET_4_5
            var jsSerializer = new JavaScriptSerializer();
            return jsSerializer.Deserialize<T>(json);
#endif
        }
    }
}