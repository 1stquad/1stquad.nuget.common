﻿namespace FirstQuad.Common.Helpers
{
    public static class JavaScriptHelper
    {
        public static string ToJsBool(this bool value)
        {
            return value ? "true" : "false";
        }
    }
}
