﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
#if NETSTANDARD2_0
using FirstQuad.Common.Classes;
#endif

namespace FirstQuad.Common.Helpers
{
    public static class SortHelper
    {
        public static IEnumerable<T> BuildOrderBys<T>(this IEnumerable<T> source, params SortDescription[] properties)
        {
            if (properties == null || properties.Length == 0) return source;

            var typeOfT = typeof(T);

            var t = typeOfT;

            IOrderedEnumerable<T> result = null;
            var thenBy = false;

            foreach (var item in properties.Select(prop => new { PropertyInfo = t.GetProperty(prop.PropertyName), prop.Direction }))
            {
                var oExpr = Expression.Parameter(typeOfT, "o");
                var propertyInfo = item.PropertyInfo;

                if (propertyInfo == null)
                    return result;

                var propertyType = propertyInfo.PropertyType;
                var isAscending = item.Direction == ListSortDirection.Ascending;

                if (thenBy)
                {
                    var prevExpr = Expression.Parameter(typeof(IOrderedEnumerable<T>), "prevExpr");
                    var expr1 = Expression.Lambda<Func<IOrderedEnumerable<T>, IOrderedEnumerable<T>>>(
                        Expression.Call((isAscending ? ThenByMethod : ThenByDescendingMethod).MakeGenericMethod(typeOfT, propertyType),
                            prevExpr,
                            Expression.Lambda(
                                typeof(Func<,>).MakeGenericType(typeOfT, propertyType),
                                Expression.MakeMemberAccess(oExpr, propertyInfo),
                                oExpr)
                            ),
                        prevExpr).Compile();

                    result = expr1(result);
                }
                else
                {
                    var prevExpr = Expression.Parameter(typeof(IEnumerable<T>), "prevExpr");
                    var expr1 = Expression.Lambda<Func<IEnumerable<T>, IOrderedEnumerable<T>>>(
                        Expression.Call(
                            (isAscending ? OrderByMethod : OrderByDescendingMethod).MakeGenericMethod(typeOfT, propertyType),
                            prevExpr,
                            Expression.Lambda(
                                typeof(Func<,>).MakeGenericType(typeOfT, propertyType),
                                Expression.MakeMemberAccess(oExpr, propertyInfo),
                                oExpr)
                            ),
                        prevExpr).Compile();

                    result = expr1(source);
                    thenBy = true;
                }
            }

            return result;
        }

        private static MethodInfo OrderByMethod = 
            MethodOf(() => Enumerable.OrderBy(default(IEnumerable<object>), default(Func<object, object>))).GetGenericMethodDefinition();

        private static MethodInfo OrderByDescendingMethod = 
            MethodOf(() => Enumerable.OrderByDescending(default(IEnumerable<object>), default(Func<object, object>))).GetGenericMethodDefinition();

        private static MethodInfo ThenByMethod = 
            MethodOf(() => Enumerable.ThenBy(default(IOrderedEnumerable<object>), default(Func<object, object>))).GetGenericMethodDefinition();

        private static MethodInfo ThenByDescendingMethod = 
            MethodOf(() => Enumerable.ThenByDescending(default(IOrderedEnumerable<object>), default(Func<object, object>))).GetGenericMethodDefinition();

        private static MethodInfo MethodOf<T>(Expression<Func<T>> method)
        {
            var mce = (MethodCallExpression)method.Body;
            var mi = mce.Method;
            return mi;
        }
    }
}
