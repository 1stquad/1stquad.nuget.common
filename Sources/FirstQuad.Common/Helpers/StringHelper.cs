﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace FirstQuad.Common.Helpers
{
    public static class StringHelper
    {
        private const string HighlightTemplate = "\t{0}\r";
        private static readonly Regex ForbiddenContentOpenTagRegex = new Regex("<[^\\s]");
        private static readonly Regex ForbiddenContentCloseTagRegex = new Regex("[^\\s]>");
        private static readonly Regex SpecialCharactersRegex = new Regex("\t|\n|\r");

        public static string DefaultIfNull(this string str, string defaultValue)
        {
            return str ?? defaultValue;
        }

        public static string DefaultIfNullOrEmpty(this string str, string defaultValue)
        {
            return string.IsNullOrEmpty(str) ? defaultValue : str;
        }

        public static string GetValueOrEmpty(this string value, string format = null)
        {
            if (format != null)
            {
                return string.IsNullOrWhiteSpace(value) ? string.Empty : string.Format(format, value);
            }

            return value ?? string.Empty;
        }

        public static string RemoveForbiddenContents(this string value)
        {
            return string.IsNullOrEmpty(value) ? value : ForbiddenContentCloseTagRegex.Replace(ForbiddenContentOpenTagRegex.Replace(value, "< "), " >");
        }

        public static string RemoveSpecialCharacters(this string value)
        {
            return string.IsNullOrEmpty(value) ? value : SpecialCharactersRegex.Replace(value, " ");
        }

        public static string GetCuttedString(this string value, int length)
        {
            value = GetValueOrEmpty(value);

            if (value.Length > length)
            {
                value = value.Substring(0, length) + "...";
            }
            return value;
        }

        public static bool IsNotNullOrEmpty(this string input)
        {
            return !string.IsNullOrEmpty(input);
        }

        public static string NormalizeNumericString(this string value)
        {
            value = value.Replace(" ", string.Empty).Replace(",", ".").Replace("'", string.Empty);

            return string.IsNullOrEmpty(value) ? "0" : value;
        }

        public static string HighlightKeywords(this string text, string keywords, string cssClass = "highlight", bool fullMatch = false)
        {
            if (string.IsNullOrEmpty(text) || string.IsNullOrEmpty(keywords) || string.IsNullOrEmpty(cssClass))
                return text;

            var terms = keywords.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(word => word.Trim());
            if (fullMatch)
                terms = terms.Select(word => "\\b" + word + "\\b");

            var res = terms.Aggregate(text, (current, pattern) =>
                   Regex.Replace(current, pattern, string.Format(HighlightTemplate, "$0"), RegexOptions.IgnoreCase));

            return res.Replace("\t", $"<span class=\"{cssClass}\">").Replace("\r", "</span>");
        }
        
        public static bool In(this string value, params string[] values)
        {
            return values.Any(x => x == value);
        }
    }
}
