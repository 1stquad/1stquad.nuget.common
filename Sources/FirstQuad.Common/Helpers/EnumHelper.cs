﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using FirstQuad.Common.Attributes;

namespace FirstQuad.Common.Helpers
{
    public static class EnumHelper
    {
        public static IList ToList(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            var list = new ArrayList();
            var enumValues = Enum.GetValues(type);

            foreach (Enum value in enumValues)
            {
                list.Add(new KeyValuePair<Enum, string>(value, GetDescription(value)));
            }

            return list;
        }

        public static string GetDescription(Type enumType, object enumerationValue)
        {
            if (enumerationValue == null)
                return string.Empty;

            if (!enumType.IsEnum)
            {
                throw new ArgumentException("EnumerationValue must be of Enum type", nameof(enumerationValue));
            }

            //Tries to find a DescriptionAttribute for a potential friendly name
            //for the enum
            MemberInfo[] memberInfo = enumType.GetMember(enumerationValue.ToString());
            if (memberInfo.Length > 0)
            {
                object[] attrs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs.Length > 0)
                {
                    //Pull out the description value
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }
            //If we have no description attribute, just return the ToString of the enum
            return enumerationValue.ToString();
        }

        public static string GetDescription(this Enum enumerationValue)
        {
            if (enumerationValue == null)
                return string.Empty;

            Type type = enumerationValue.GetType();
            return GetDescription(type, enumerationValue);
        }

        public static IEnumerable<string> GetAllDescriptions<TEnum>(params TEnum[] notToBeMappedExclusions)
        {
            var values = (TEnum[])Enum.GetValues(typeof(TEnum));
            if (notToBeMappedExclusions != null && notToBeMappedExclusions.Length > 0)
            {
                values = values.Where(m => !notToBeMappedExclusions.Contains(m)).ToArray();
            }
            return values.Select(m => GetDescription(typeof(TEnum), m));
        }

        public static List<object> ToObjectsList<TEnum>(this List<TEnum> enumValues) where TEnum : struct, IConvertible, IComparable, IFormattable
        {
            return enumValues.Cast<object>().ToList();
        }

        public static string GetCategory(this Enum enumerationValue)
        {
            if (enumerationValue == null)
                return string.Empty;
            Type type = enumerationValue.GetType();
            if (!type.IsEnum)
            {
                throw new ArgumentException("EnumerationValue must be of Enum type", nameof(enumerationValue));
            }

            //Tries to find a CategoryAttribute for a potential friendly name
            //for the enum
            MemberInfo[] memberInfo = type.GetMember(enumerationValue.ToString());
            if (memberInfo.Length > 0)
            {
                object[] attrs = memberInfo[0].GetCustomAttributes(typeof(CategoryAttribute), false);

                if (attrs.Length > 0)
                {
                    //Pull out the category value
                    return ((CategoryAttribute)attrs[0]).Category;
                }
            }
            //If we have no category attribute, just return the ToString of the enum
            return enumerationValue.ToString();
        }

        public static T GetAttribute<T>(this Enum enumerationValue) where T : Attribute
        {
            if (enumerationValue == null)
                return null;

            var type = enumerationValue.GetType();
            if (!type.IsEnum)
            {
                throw new ArgumentException("EnumerationValue must be of Enum type", nameof(enumerationValue));
            }

            var memberInfo = type.GetMember(enumerationValue.ToString());
            if (memberInfo.Length > 0)
            {
                var attrs = memberInfo[0].GetCustomAttributes(typeof(T), false);

                if (attrs.Length > 0)
                {
                    return (T)attrs[0];
                }
            }

            return null;
        }

        public static bool In<T>(this T val, params T[] values) where T : struct
        {
            var list = new List<T>(values);
            return list.Contains(val);
        }

        public static bool In<T>(this T? val, params T[] values) where T : struct
        {
            if (!val.HasValue)
                return false;

            var list = new List<T>(values);
            return list.Contains(val.Value);
        }

        public static bool In<T>(this int val, params T[] values) where T : struct
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            return new List<T>(values).Contains((T)(ValueType)val);
        }

        public static T[] ToBitEnums<T>(this int val) where T : struct
        {
            var items = new List<T>();

            var possibleValues = Enum.GetValues(typeof(T)).Cast<T>();
            foreach (var value in possibleValues)
            {
                if ((val & Convert.ToInt32(value)) > 0)
                    items.Add(value);
            }

            return items.ToArray();
        }

        public static bool IsNullableEnum(this Type t)
        {
            Type u = Nullable.GetUnderlyingType(t);
            return (u != null) && u.IsEnum;
        }

        public static int GetOrder(this Enum enumerationValue)
        {
            if (enumerationValue == null)
                return Int16.MaxValue;

            Type type = enumerationValue.GetType();
            if (!type.IsEnum)
            {
                throw new ArgumentException("EnumerationValue must be of Enum type", nameof(enumerationValue));
            }

            //Tries to find a DescriptionAttribute for a potential friendly name
            //for the enum
            MemberInfo[] memberInfo = type.GetMember(enumerationValue.ToString());
            if (memberInfo.Length > 0)
            {
                object[] attrs = memberInfo[0].GetCustomAttributes(typeof(OrderAttribute), false);

                if (attrs.Length > 0)
                {
                    //Pull out the description value
                    return ((OrderAttribute)attrs[0]).Order;
                }
            }
            //If we have no description attribute, just return the ToString of the enum
            return Int16.MaxValue + Convert.ToInt32(enumerationValue);
        }
    }
}
