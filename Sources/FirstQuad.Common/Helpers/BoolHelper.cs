﻿using System.Linq;

namespace FirstQuad.Common.Helpers
{
    public static class BoolHelper
    {
        public static int TrueCount(this bool[] array)
        {
            return array.Count(t => t);
        }
    }
}
