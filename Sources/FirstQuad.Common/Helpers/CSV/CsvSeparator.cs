﻿using System.ComponentModel;

namespace FirstQuad.Common.Helpers.CSV
{
    public enum CsvSeparator
    {
        [Description("Semi-colon (;)")]
        SemiColon = 0,
        [Description("Tab")]
        Tab = 1,
        [Description("Comma (,)")]
        Comma = 2,
        [Description("Pipeline (|)")]
        Pipeline = 3,

    }
}
