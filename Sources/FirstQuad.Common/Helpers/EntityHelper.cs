﻿using System;

namespace FirstQuad.Common.Helpers
{
    public static class EntityHelper
    {
        public static Type GetEntityType(this Type entityType)
        {
            var type = entityType;
            while (type != null && type.BaseType != typeof(object))
            {
                type = type.BaseType;
            }
            if (type == null)
                throw new InvalidProgramException("type is null");

            return type;
        }
    }
}
