using System;

namespace FirstQuad.Common.Enums
{
    [Flags]
    public enum CollectionManageType
    {
        Add = 1,
        Remove = 2,
        All = Add | Remove
    }
}