using System;
using System.Collections;
using System.Collections.Generic;

namespace FirstQuad.Common.Collections
{
    public enum CycleBufferResizeStrategy
    {
        AutoExpand = 0,
        ThrowException = 1,
        CycleRewrite = 2
    }

    public class CycleBuffer<T>: IEnumerable
    {
        private T[] _items;
        private int _currentStart;
        private int _currentEnd = -1;
        private int _length;
        internal int _version;        // Used to keep enumerator in sync w/ collection.
        private readonly CycleBufferResizeStrategy _resizeStrategy;

        public bool HasOverflowExists { get; private set; }

        public CycleBuffer(int initialSize, CycleBufferResizeStrategy resizeStrategy = CycleBufferResizeStrategy.AutoExpand)
        {
            _items = new T[initialSize];
            _resizeStrategy = resizeStrategy;
        }
		
		public CycleBuffer(): this(4)
        {
        }

        public void Sort(IComparer<T> comparer)
        {
            //make array zero-based
            Resize(_items.Length);
            //sort only filled space
            Array.Sort(_items, 0, _length, comparer);
        }

        private void Resize(int newSize)
        {
            var newItems = new T[newSize];
            for (var i = 0; i < _length; i++)
            {
                newItems[i] = this[i];
            }
            _currentStart = 0;
            _currentEnd = _length - 1;
            _items = newItems;
            _version++;
        }

        public T this[int zeroBasedIndex]
        {
            get
            {
                if (zeroBasedIndex >= _length)
                    throw new InvalidProgramException($"Index {zeroBasedIndex} out of range buffer length {_length}");

                var index = (_currentStart + zeroBasedIndex) % _items.Length;

                return _items[index];
            }
            set
            {
                if (zeroBasedIndex >= _length)
                    throw new InvalidProgramException($"Index {zeroBasedIndex} out of range buffer length {_length}");

                var index = (_currentStart + zeroBasedIndex) % _items.Length;
                _items[index] = value;
            	_version++;
            }
        }

        public int Length => _length;

        public void Push(T item)
        {
            if (_length == _items.Length)
            {
                if (_resizeStrategy == CycleBufferResizeStrategy.ThrowException)
                {
                    throw new InvalidProgramException($"Buffer overflow with size: {_items.Length}");
                }

                if (_resizeStrategy == CycleBufferResizeStrategy.AutoExpand)
                {
	                var newSize = _length * 2;
	                if (newSize == 0)
	                {
	                    newSize = 4;
	                }
	                Resize(newSize);
                }

                if (_resizeStrategy == CycleBufferResizeStrategy.CycleRewrite)
                {
                    HasOverflowExists = true;
                    PopStart();
                }
            }

            _length++;
            _currentEnd = (_currentEnd + 1) % _items.Length;

            _items[_currentEnd] = item;
            _version++;
        }

        public ref T PeekStart
        {
            get
            {
                if (_length == 0)
                    throw new InvalidProgramException($"Buffer is empty");

                return ref _items[_currentStart];
            }
        }

        public T PeekEnd
        {
            get
            {
                if (_length == 0)
                    throw new InvalidProgramException($"Buffer is empty");

                return _items[_currentEnd];
            }
        }

        public T PopStart()
        {
            if (_length == 0)
                throw new InvalidProgramException($"Buffer is empty");

            var result = _items[_currentStart];
            _length--;
            _currentStart = (_currentStart + 1) % _items.Length;
            _version++;
            return result;
        }

        public void Clean()
        {
            _length = 0;
            _currentStart = 0;
            _currentEnd = _length - 1;
            _version++;
            HasOverflowExists = false;
        }

        #region IEnumerator

        public IEnumerator GetEnumerator()
        {
            return new CycleBufferEnumerator<T>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }

    [Serializable]
    public class CycleBufferEnumerator<T> : IEnumerator, ICloneable
    {
        private CycleBuffer<T> _cycleBuffer;
        private int _index;
        private int _version;
        private object _currentElement;

        internal CycleBufferEnumerator(CycleBuffer<T> cycleBuffer)
        {
            _cycleBuffer = cycleBuffer;
            _version = _cycleBuffer._version;
            _index = -2;
            _currentElement = null;
        }

        public Object Clone()
        {
            return MemberwiseClone();
        }

        public virtual bool MoveNext()
        {
            bool retval;
            if (_version != _cycleBuffer._version) throw new InvalidOperationException($"Enumeration failed: version mismatch {_version} vs {_cycleBuffer._version}. Do not modify collection during enumeration");

            if (_index == -2)
            {  // First call to enumerator.
                _index = 0;
                retval = _index < _cycleBuffer.Length - 1;
                if (retval)
                    _currentElement = _cycleBuffer[_index];
                return retval;
            }

            if (_index == -1)
            {  // End of enumeration.
                return false;
            }

            retval = (++_index <= _cycleBuffer.Length - 1);
            if (retval)
                _currentElement = _cycleBuffer[_index];
            else
                _currentElement = null;
            return retval;
        }

        public virtual Object Current
        {
            get
            {
                if (_index == -2) throw new InvalidOperationException("Enumeration is not started yet");
                if (_index == -1) throw new InvalidOperationException("Enumeration already finished");
                return _currentElement;
            }
        }

        public virtual void Reset()
        {
            if (_version != _cycleBuffer._version) throw new InvalidOperationException($"Enumeration failed: version mismatch {_version} vs {_cycleBuffer._version}. Do not modify collection during enumeration");
            _index = -2;
            _currentElement = null;
        }
    }
}
