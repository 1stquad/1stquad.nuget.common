﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace FirstQuad.Common.Utils
{
    public static class AttributeUtils
    {
        public static string GetPropertyDisplayName(string fullPropertyName)
        {
            if (!string.IsNullOrEmpty(fullPropertyName))
            {
                var fieldLiterals = fullPropertyName.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToArray();
                if (fieldLiterals.Length == 3)
                {
                    var typeName = fieldLiterals[0];
                    var assemblyName = fieldLiterals[1];
                    var propertyName = fieldLiterals[2];
                    var fullyQualifiedTypeName = string.Format("{0}, {1}", typeName, assemblyName);
                    var type = Type.GetType(fullyQualifiedTypeName);
                    if (type != null)
                    {
                        var propertyInfo = type.GetProperty(propertyName);
                        var displayNameAttribute = propertyInfo.GetCustomAttribute<DisplayNameAttribute>();

                        if (displayNameAttribute != null)
                        {
                            return displayNameAttribute.DisplayName;
                        }
                    }

                    return propertyName;
                }
            }

            return string.Empty;
        }
    }
}
