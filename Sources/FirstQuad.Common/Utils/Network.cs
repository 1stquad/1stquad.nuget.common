﻿#if NET_4_5
using System;
using System.Net;
using System.Net.Sockets;
using System.Web;

namespace FirstQuad.Common.Utils
{
    public static class Network
    {
        public static string GetIp4Address()
        {
            var context = HttpContext.Current;

            if (context != null && context.Request.UserHostAddress != null)
            {
                foreach (var ip in Dns.GetHostAddresses(context.Request.UserHostAddress))
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        return ip.ToString();
                    }
                }
            }

            foreach (var ip in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork && !IPAddress.IsLoopback(ip))
                {
                    return ip.ToString();
                }
            }

            throw new ApplicationException($"IP address v4 not found. Host [{Dns.GetHostName()}]");
        }
    }
}
#endif