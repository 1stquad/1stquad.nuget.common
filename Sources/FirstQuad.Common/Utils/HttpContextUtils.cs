﻿#if NET_4_5

using System.IO;
using System.Reflection;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.SessionState;

namespace FirstQuad.Common.Utils
{
    public static class HttpContextUtils
    {
        public static HttpContext CreateNewStub(string fakeUrl)
        {
            var response = new HttpResponse(new StringWriter());

            var context = new HttpContext(new HttpRequest("", fakeUrl, ""), response);
            context.ApplicationInstance = new HttpApplication();
            context.ApplicationInstance.GetType().GetField("_context", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(context.ApplicationInstance, context);
            var state = (HttpSessionState)System.Runtime.Serialization.FormatterServices.GetUninitializedObject(typeof(HttpSessionState));
            var containerFld = typeof(HttpSessionState).GetField("_container", BindingFlags.Instance | BindingFlags.NonPublic);
            var itemCollection = new SessionStateItemCollection();
            containerFld.SetValue(state,
                new HttpSessionStateContainer("1",
                    itemCollection,
                    new HttpStaticObjectsCollection(),
                    900,
                    true,
                    HttpCookieMode.UseCookies,
                    SessionStateMode.InProc,
                    false
                ));
            context.Items["AspSession"] = state;
            Thread.CurrentPrincipal = new WindowsPrincipal(WindowsIdentity.GetCurrent());

            return context;
        }
    }
}

#endif