﻿using System;
using System.ComponentModel;

namespace FirstQuad.Common.Classes
{
    public struct SortDescription
    {
        public string PropertyName { get; set; }
        public ListSortDirection Direction { get; set; }

        public SortDescription(string propertyName, ListSortDirection direction)
        {
            if (propertyName == null)
            {
                throw new ArgumentNullException();
            }
            if (String.IsNullOrWhiteSpace(propertyName))
            {
                throw new ArgumentException("Parameter cannot be empty");
            }
            PropertyName = propertyName;
            Direction = direction;
        }
    }
}
