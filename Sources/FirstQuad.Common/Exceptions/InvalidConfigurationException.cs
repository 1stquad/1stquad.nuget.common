﻿using System;

namespace FirstQuad.Common.Exceptions
{
    public class InvalidConfigurationException : BaseException
    {
        public InvalidConfigurationException() : base(string.Empty) { }

        public InvalidConfigurationException(string message) : base(message) { }
        
        public InvalidConfigurationException(string message, Exception innerException)
            : base(message, null, innerException) { }
    }
}