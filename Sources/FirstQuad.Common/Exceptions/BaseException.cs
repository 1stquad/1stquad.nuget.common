﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace FirstQuad.Common.Exceptions
{
    [Serializable]
    public class BaseException : ApplicationException
    {
        private string _additionalInfo = string.Empty;

        protected BaseException(string message) : base(message) { }

        protected BaseException(string message, string additionalInfo) : base(message)
        {
            SetAdditionalInfo(additionalInfo);
        }

        protected BaseException(string message, string additionalInfo, Exception innerException)
            : base(message, innerException)
        {
            SetAdditionalInfo(additionalInfo);
        }

        public string AdditionalInfo => _additionalInfo;

        protected void SetAdditionalInfo(string info)
        {
            _additionalInfo = info;
        }

        public override string ToString()
        {
            var content = base.ToString();

            if (AdditionalInfo == null)
                return content;

            content += Environment.NewLine;
            content += AdditionalInfo;

            return content;
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException(nameof(info));

            info.AddValue("AdditionalInfo", AdditionalInfo);
            base.GetObjectData(info, context);
        }
    }
}
