﻿using System;

namespace FirstQuad.Common.Exceptions
{
    [Serializable]
    public class UiMessageException : BaseException
    {
        public UiMessageException() : base(string.Empty) { }

        public UiMessageException(string message) : base(message) { }

        public UiMessageException(string message, string title)
            : base(message, title) { }

        public UiMessageException(string message, string title, Exception innerException)
            : base(message, title, innerException) { }
    }
}
